package model.entities;

import com.mobandme.ada.Entity;
import com.mobandme.ada.annotations.TableField;

public class User extends Entity {
	@TableField(name = "nick", datatype = DATATYPE_STRING)
	public String nick;
	@TableField(name = "tlf", datatype = DATATYPE_STRING)
	public String tlf;
	@TableField(name = "token", datatype = DATATYPE_STRING)
	public String token;
	@TableField(name = "publicKey", datatype = DATATYPE_STRING)
	public String publicKey;
	@TableField(name = "privateKey", datatype = DATATYPE_STRING)
	public String privateKey;

	public User() {
	}

}
