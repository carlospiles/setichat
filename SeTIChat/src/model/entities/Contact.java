package model.entities;

import java.util.ArrayList;
import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import com.mobandme.ada.Entity;
import com.mobandme.ada.annotations.Table;
import com.mobandme.ada.annotations.TableField;

@Table(name = "Contact")
@Root(name = "contact")
public class Contact extends Entity {
	@Element(name = "nick")
	@TableField(name = "nick", datatype = DATATYPE_STRING)
	public String nick;

	@Element(name = "mobile")
	@TableField(name = "tlf", datatype = DATATYPE_STRING)
	public String tlf;

	@TableField(name = "messages", datatype = DATATYPE_ENTITY)
	public List<ChatMessage> messages;

	@TableField(name = "publicKey", datatype = DATATYPE_STRING)
	public String publicKey;

	@TableField(name = "isRevocated", datatype = DATATYPE_BOOLEAN)
	public boolean isRevocated;

	public Contact() {
		this.isRevocated = true;
		this.messages = new ArrayList<ChatMessage>();
	}

	public void init() {
		this.isRevocated = true;
		this.messages = new ArrayList<ChatMessage>();
	}

	@Override
	public String toString() {
		return "(" + nick + ")" + " " + tlf;
	}

}
