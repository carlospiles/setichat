package model.entities;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name = "message")
@Order(elements = { "header", "content" })
public class Connection implements Content {
	@Element(name = "header")
	public Header header;

	@Path("content")
	@Element(name = "connection")
	public String connection;

	public Connection(Header header, String connection) {
		this.header = header;
		this.connection = connection;
	}

	public Connection() {
		super();
	}
}
