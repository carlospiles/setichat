package model.entities;

import org.simpleframework.xml.Element;

public class Header {

	@Element
	private String idSource;

	@Element
	private String idDestination;

	@Element
	private String idMessage;

	@Element
	private int type;

	@Element
	private boolean encrypted;

	@Element
	private boolean signed;

	public Header() {
		super();
	}

	public Header(String idSource, String idDestination, String idMessage,
			int type, boolean encrypted, boolean signed) {
		this.setIdSource(idSource);
		this.idDestination = idDestination;
		this.idMessage = idMessage;
		this.encrypted = encrypted;
		this.signed = signed;
		this.type = type;
	}

	public String getIdMessage() {
		return idMessage;
	}

	public void setIdMessage(String idMessage) {
		this.idMessage = idMessage;
	}

	public void setIdDestination(String idDestination) {
		this.idDestination = idDestination;
	}

	public int getType() {
		return type;
	}

	public String getIdDestination() {
		return idDestination;
	}

	public String getIdSource() {
		return idSource;
	}

	public void setIdSource(String idSource) {
		this.idSource = idSource;
	}

	public boolean getEncrypted() {
		return encrypted;
	}

	public boolean getSigned() {
		return signed;
	}
}
