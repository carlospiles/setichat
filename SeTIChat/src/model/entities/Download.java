package model.entities;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name = "message")
@Order(elements = { "header", "content" })
public class Download implements Content {
	@Element(name = "header")
	public Header header;

	@Path("content/download")
	@Element(name = "key")
	public String key;

	@Path("content/download")
	@Element(name = "type")
	public String type;

	@Path("content/download")
	@Element(name = "mobile")
	public String mobile;

	public Download(Header header, String key, String type, String mobile) {
		this.header = header;
		this.type = type;
		this.mobile = mobile;
		this.key = key;
	}

	public Download() {
		super();
	}
}
