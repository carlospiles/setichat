package model.entities;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name = "message")
@Order(elements = { "header", "content" })
public class SignUp implements Content {

	@Element
	public Header header;

	@Path("content/signup")
	@Element
	private String nick;

	@Path("content/signup")
	@Element
	private String mobile;

	public SignUp() {
		super();
	}

	public SignUp(Header header, String nick, String mobile) {
		this.header = header;
		this.nick = nick;
		this.mobile = mobile;
	}
}
