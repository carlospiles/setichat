package model.entities;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import com.mobandme.ada.Entity;
import com.mobandme.ada.annotations.TableField;

@Root(name = "message")
@Order(elements = { "header", "content", "signature" })
public class ChatMessage extends Entity implements Content {
	@Element(name = "header")
	public Header header;

	@TableField(name = "origin", datatype = DATATYPE_STRING)
	public String origin;

	@Path("content")
	@Element(name = "chatMessage")
	@TableField(name = "message", datatype = DATATYPE_STRING)
	public String message;

	@Element(name = "signature", required = false)
	public String signature;

	public ChatMessage(Header header, String message) {
		this.header = header;
		this.message = message;
		this.signature = null;
	}

	public ChatMessage(Header header, String message, String signature) {
		this.header = header;
		this.message = message;
		this.signature = signature;
	}

	public ChatMessage() {
		super();
	}

	@Override
	public String toString() {
		return message;
	}
}
