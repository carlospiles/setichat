package es.uc3m.setichat.helpers;

import java.util.ArrayList;
import java.util.Collections;

import model.entities.ContactTlf;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Log;

import com.mobandme.ada.exceptions.AdaFrameworkException;

/**
 * @author Cesar de la Vega
 * 
 */
public class ContactsHelper {
	Context context;

	public ContactsHelper(Context context) {
		super();
		this.context = context;
	}

	/**
	 * This method returns a String array with all the contacts with phone
	 * number
	 * 
	 * @param context
	 *            Activity context that is using this helper
	 * @return The array with all the numbers
	 */
	public ArrayList<String> getAllContacts() {
		String[] projection = new String[] { Phone.NUMBER };
		Cursor phones = context.getContentResolver().query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection,
				null, null, null);
		String contactNumbers[] = new String[phones.getCount()];
		ArrayList<String> contactNumbersList = new ArrayList<String>();
		int i = 0;
		while (phones.moveToNext()) {
			ArrayList<ContactTlf> contactsDB = getContactsFromDB(context);
			ContactTlf aux = new ContactTlf();
			String number = phones
					.getString(phones
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			aux.tlf = number;
			if (!contactsDB.contains(aux)) {
				contactNumbers[i] = number;
				i++;
			}

		}
		phones.close();
		Collections.addAll(contactNumbersList, contactNumbers);
		return contactNumbersList;
	}

	private ArrayList<ContactTlf> getContactsFromDB(Context context) {
		try {
			DatabaseHelper dbhelper = new DatabaseHelper(context);
			dbhelper.contactTlfSet.fill();
			return dbhelper.contactTlfSet;

		} catch (AdaFrameworkException e) {
			e.printStackTrace();
			Log.e("DB", "Error leyendo contactos de la base de datos");
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("DB", "Error de base de datos");
		}
		return null;
	}

}
