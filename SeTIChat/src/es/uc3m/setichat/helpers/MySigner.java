package es.uc3m.setichat.helpers;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import es.uc3m.setichat.utils.Base64;

public class MySigner {
	/**
	 * Firma un mensaje a partir de los parámetros que forman la firma
	 * 
	 * @return string a partir de la firma en base64
	 */
	public String sign_message(String idDestination, String idMessage,
			String content, String ourPrivateKey) throws Exception {
		String to_sign = "<idDestination>" + idDestination + "</idDestination>"
				+ "<idMessage>" + idMessage + "</idMessage>"
				+ "<content><chatMessage>" + content
				+ "</chatMessage></content>";
		byte[] privateKeyBytes = Base64.decode(ourPrivateKey);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		KeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
		PrivateKey pKey = keyFactory.generatePrivate(privateKeySpec);

		Signature sign = Signature.getInstance("SHA1withRSA");
		sign.initSign(pKey);
		sign.update(to_sign.getBytes("UTF-8"));
		byte[] signatureResult = sign.sign();

		return es.uc3m.setichat.utils.Base64.encodeToString(signatureResult,
				false);
	}

	/**
	 * Comprueba una firma
	 * 
	 * @return boolean
	 */
	public boolean check_signature(String idDestination, String idMessage,
			String content, String signature_64, String hisPublicKey) {
		byte[] signature = Base64.decode(signature_64);
		String to_verify = "<idDestination>" + idDestination
				+ "</idDestination>" + "<idMessage>" + idMessage
				+ "</idMessage>" + "<content><chatMessage>" + content
				+ "</chatMessage></content>";
		byte[] publicKeyBytes = Base64.decode(hisPublicKey);
		KeyFactory keyFactory = null;
		X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(
				publicKeyBytes);
		java.security.interfaces.RSAPublicKey pKey = null;
		try {
			keyFactory = KeyFactory.getInstance("RSA");
			pKey = (RSAPublicKey) keyFactory.generatePublic(publicKeySpec);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}

		Signature sign = null;
		try {
			sign = Signature.getInstance("SHA1withRSA");
			sign.initVerify(pKey);
			sign.update(to_verify.getBytes());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		}
		try {
			return sign.verify(signature);
		} catch (SignatureException e) {
			return false;
		}
	}
}