package es.uc3m.setichat.helpers;

import java.util.List;

import model.entities.Contact;
import model.entities.ContactTlf;
import model.entities.User;
import android.content.Context;

import com.mobandme.ada.ObjectContext;
import com.mobandme.ada.ObjectSet;
import com.mobandme.ada.annotations.ObjectSetConfiguration;
import com.mobandme.ada.exceptions.AdaFrameworkException;

/**
 * 
 * @author Carlos Munoz
 * 
 */
public class DatabaseHelper extends ObjectContext {
	public ObjectSet<Contact> contactSet;
	@ObjectSetConfiguration(virtual = true)
	public ObjectSet<ContactTlf> contactTlfSet;
	public ObjectSet<User> UserSet;

	public DatabaseHelper(Context pContext) throws Exception {
		super(pContext, "setichat.db");

		if (contactSet == null) {
			this.contactSet = new ObjectSet<Contact>(Contact.class, this);
		}
		if (contactTlfSet == null) {
			this.contactTlfSet = new ObjectSet<ContactTlf>(ContactTlf.class,
					this);
		}
		if (UserSet == null) {
			this.UserSet = new ObjectSet<User>(User.class, this);
		}
	}

	// METODOS RELACIONADOS CON LA DATABASE AQUI!
	public Contact getContactbySourceId(String phone)
			throws AdaFrameworkException {
		String wherePattern = String.format("%s = ?",
				contactSet.getDataTableFieldName("tlf"));
		contactSet.fill(wherePattern, new String[] { "" + phone }, null);
		return contactSet.get(0);
	}

	public boolean isContactInDatabase(String phone)
			throws AdaFrameworkException {
		String wherePattern = String.format("%s = ?",
				contactSet.getDataTableFieldName("tlf"));
		List<Contact> a = contactSet.search("Contact", false, null,
				wherePattern, new String[] { "" + phone }, null, null, null,
				null, null);
		if (a == null) {
			return false;
		} else {
			return true;
		}
	}

}
