package es.uc3m.setichat.helpers;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fima.cardsui.objects.RecyclableCard;

import es.uc3m.setichat.R;

public class MyCard extends RecyclableCard {

	public MyCard(String title, String description) {
		super(title, description);
	}

	public MyCard(String nick, String message, String string, String string2,
			boolean b, boolean c) {
		super(nick, message, string, string2, b, c);
	}

	@Override
	protected void applyTo(View convertView) {
		((TextView) convertView.findViewById(R.id.title)).setText(titlePlay);
		((TextView) convertView.findViewById(R.id.title)).setTextColor(Color
				.parseColor(titleColor));
		((TextView) convertView.findViewById(R.id.description))
				.setText(description);
		((ImageView) convertView.findViewById(R.id.stripe))
				.setBackgroundColor(Color.parseColor(color));
	}

	@Override
	protected int getCardLayoutId() {
		return R.layout.card_ex;
	}

}
