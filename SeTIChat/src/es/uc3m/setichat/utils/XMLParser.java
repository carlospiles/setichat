package es.uc3m.setichat.utils;

import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

import model.entities.ChatMessage;
import model.entities.Connection;
import model.entities.ContactRequest;
import model.entities.ContactResponse;
import model.entities.Content;
import model.entities.Download;
import model.entities.FirstHeader;
import model.entities.Header;
import model.entities.KeyRequest;
import model.entities.Response;
import model.entities.Revocation;
import model.entities.SignUp;
import model.entities.Upload;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.util.Log;

/**
 * @author Borja Navas
 * 
 */
public class XMLParser {
	// http://xjaphx.wordpress.com/2011/10/27/android-xml-adventure-create-write-xml-data/
	public String generateMessage(Header header, Content content) {

		String xmlMessage = "";

		// have the object build the directory structure, if needed.
		Serializer serializer = new Persister();

		switch (header.getType()) {
		case 1:
			SignUp content1 = (SignUp) content;
			content1.header = header;
			content = content1;
			break;
		case 2:
			ContactRequest content2 = (ContactRequest) content;
			content2.header = header;
			content = content2;
			break;
		case 4:
			ChatMessage content4 = (ChatMessage) content;
			content4.header = header;
			content = content4;
			break;
		case 5:
			Connection content5 = (Connection) content;
			content5.header = header;
			content = content5;
			break;
		case 9:
			Upload content9 = (Upload) content;
			content9.header = header;
			content = content9;
			break;
		case 10:
			KeyRequest content10 = (KeyRequest) content;
			content10.header = header;
			content = content10;
			break;

		default:
			break;
		}
		Writer writer = new StringWriter();
		try {
			serializer.write(content, writer);
			xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
					+ writer.toString();
			xmlMessage = xmlMessage.replaceAll("\\>\\s+\\<", "><");
			xmlMessage = xmlMessage.replaceAll("\t", "");
			Log.e("mensagear_sintabular", xmlMessage);

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(xmlMessage);
		return xmlMessage;

	}

	public Response readMessage(String respuesta) {
		Serializer serializer = new Persister();
		Response example = new Response();
		Reader reader = new StringReader(respuesta);
		try {
			example = serializer.read(Response.class, reader);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return example;
	}

	public ChatMessage readChatMessage(String respuesta) {
		Serializer serializer = new Persister();
		ChatMessage example = new ChatMessage();
		Reader reader = new StringReader(respuesta);
		try {
			example = serializer.read(ChatMessage.class, reader);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return example;
	}

	public ContactResponse readContactResponse(String respuesta) {
		Serializer serializer = new Persister();
		ContactResponse example = new ContactResponse();
		Reader reader = new StringReader(respuesta);
		try {
			example = serializer.read(ContactResponse.class, reader);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return example;
	}

	public Revocation readRevocation(String respuesta) {
		Serializer serializer = new Persister();
		Revocation revocation = new Revocation();
		Reader reader = new StringReader(respuesta);

		try {
			revocation = serializer.read(Revocation.class, reader);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return revocation;
	}

	public int getTypeMessage(String respuesta) {
		Serializer serializer = new Persister();
		Reader reader = new StringReader(respuesta);
		FirstHeader header = new FirstHeader();
		try {
			header = serializer.read(FirstHeader.class, reader);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return header.getType();
	}

	public Download readDownolad(String respuesta) {
		Serializer serializer = new Persister();
		Reader reader = new StringReader(respuesta);
		Download download = new Download();
		try {
			download = serializer.read(Download.class, reader);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return download;
	}
}
