package es.uc3m.setichat.activity;

import java.util.ArrayList;
import java.util.UUID;

import model.entities.SignUp;
import model.entities.User;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.mobandme.ada.Entity;
import com.mobandme.ada.exceptions.AdaFrameworkException;

import es.uc3m.setichat.R;
import es.uc3m.setichat.helpers.ContactsHelper;
import es.uc3m.setichat.helpers.DatabaseHelper;
import es.uc3m.setichat.helpers.MyCipher;
import es.uc3m.setichat.service.SeTIChatService;
import es.uc3m.setichat.service.SeTIChatServiceBinder;
import es.uc3m.setichat.utils.Base64;
import es.uc3m.setichat.utils.XMLParser;

/**
 * This is the main activity and its used to initialize all the SeTIChat
 * features. It configures the three tabs used in this preliminary version of
 * SeTIChat. It also start the service that connects to the SeTIChat server.
 * 
 * @author Guillermo Suarez de Tangil <guillermo.suarez.tangil@uc3m.es>
 * @author Jorge Blasco Alis <jbalis@inf.uc3m.es>
 */

public class MainActivitySeTIChat extends FragmentActivity implements
		ActionBar.TabListener {

	// constants
	static final int ACTION_SIGN_UP = 0;

	boolean registered = false;
	boolean first_tab = true;
	boolean first_login = false;

	// Saved Preferences (only for managing first init)
	SharedPreferences sharedPref;

	// database
	DatabaseHelper dbhelper;
	User usuario;

	// parser
	XMLParser parser;
	// Service used to access the SeTIChat server
	private SeTIChatService mService;
	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";

	// Receivers that wait for notifications from the SeTIChat server
	private BroadcastReceiver openReceiver;
	private BroadcastReceiver chatMessageReceiver;
	private BroadcastReceiver createAccountReceiver;

	@Override
	protected void onPause() {
		super.onPause();
		unregisterAllReceivers();
		Log.e("MainActivity", "onpause");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		sharedPref = this.getSharedPreferences("PREFS", Context.MODE_PRIVATE);
		first_login = sharedPref.getBoolean("my_first_time", true);

		Log.e("CICLO", "oncreate");
		// Si existe el usuario en la BBDD, registered=true, sino,
		// registered=false
		leerContactoDatabase();
		parser = new XMLParser();
		//
		if (registered) {
			try {
				// Si el usuario esta registrado, lanzamos el servicio

				startService(new Intent(MainActivitySeTIChat.this,
						SeTIChatService.class));

				bindService(new Intent(MainActivitySeTIChat.this,
						SeTIChatService.class), mConnection,
						Context.BIND_AUTO_CREATE);
			} catch (Exception e) {

				Log.d("MainActivity", "Unknown Error", e);

				stopService(new Intent(MainActivitySeTIChat.this,
						SeTIChatService.class));
			}
		} else {
			// Sino, lanzamos la activiy createAccount que nos devolvera nick y
			// phone.
			startActivityForResult(new Intent(MainActivitySeTIChat.this,
					CreateAccount.class), ACTION_SIGN_UP);
		}

		// Set up the action bar to show tabs.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		// For each of the sections in the app, add a tab to the action bar.
		actionBar.addTab(actionBar.newTab().setText("Contactos")
				.setTabListener(this));
		actionBar.addTab(actionBar.newTab().setText("Extra1")
				.setTabListener(this));
		actionBar.addTab(actionBar.newTab().setText("Extra2")
				.setTabListener(this));
	}

	private void leerContactoDatabase() {
		// lee el user de la database, si no hay no se ha registrado.
		try {
			dbhelper = new DatabaseHelper(getApplicationContext());

			dbhelper.UserSet.fill();
			if (dbhelper.UserSet.isEmpty()) {
				registered = false;
			} else {
				registered = true;
				usuario = dbhelper.UserSet.get(0);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	private void startSetichat() {
		// Find new contacts
		SendMessageTask msgtask = new SendMessageTask();
		msgtask.execute();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.e("MainActivity", "ondestroy");

		// We stop the service if activity is destroyed
		try {
			stopService(new Intent(MainActivitySeTIChat.this,
					SeTIChatService.class));
		} catch (Exception e) {
		}
		// We also unregister the receivers to avoid leaks.
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.e("MainActivity", "onstop");
		try {
			unbindService(mConnection);
		} catch (Exception e) {
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.e("MainActivity", "onresume");

		registerAllReceivers();
	}

	// Funcion que elimina todos los broadcast receiver
	private void unregisterAllReceivers() {
		try {
			unregisterReceiver(openReceiver);
			unregisterReceiver(chatMessageReceiver);
			unregisterReceiver(createAccountReceiver);
		} catch (Exception e) {
		}
	}

	// Funcion que crea todos los broadcast receiver
	private void registerAllReceivers() {
		Log.v("MainActivity", "onResume: Resuming activity...");
		// Create and register broadcast receivers

		// Broadcast que recibe la activity cuando el servicio recibe el
		// connected del servidor
		openReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Context context1 = getApplicationContext();
				CharSequence text = "SeTIChatConnected";
				int duration = Toast.LENGTH_SHORT;

				Toast toast = Toast.makeText(context1, text, duration);
				toast.show();

				if (!registered) {
					model.entities.Header header = new model.entities.Header(
							usuario.tlf, "setichat.appspot.com", UUID
									.randomUUID().toString(), 1, false, false);
					model.entities.SignUp signUpContent = new SignUp(header,
							usuario.nick, usuario.tlf);
					mService.sendMessage(parser.generateMessage(header,
							signUpContent));
				} else {
					startSetichat();
				}
			}
		};

		IntentFilter openFilter = new IntentFilter();
		openFilter.addAction("es.uc3m.SeTIChat.CHAT_OPEN");
		registerReceiver(openReceiver, openFilter);

		// Broadcast que recibe la activity cuando le llega un chatMessage al
		// servicio
		chatMessageReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {

				Context context1 = getApplicationContext();
				int duration = Toast.LENGTH_LONG;
				Toast toast = Toast.makeText(context1,
						intent.getStringExtra("message"), duration);
				toast.show();

			}
		};
		IntentFilter chatMessageFilter = new IntentFilter();
		chatMessageFilter.addAction("es.uc3m.SeTIChat.CHAT_MESSAGE");
		registerReceiver(chatMessageReceiver, chatMessageFilter);

		// Broadcast que recibe la activity cuando el servicio confirma con el
		// servidor el resultado del registro
		createAccountReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {

				switch (intent.getIntExtra("code", 201)) {
				case 406:
					finish();
					startActivity(getIntent());
					break;
				case 407:
					startActivity(getIntent());
					break;
				default:
					if (first_login) {
						Toast.makeText(MainActivitySeTIChat.this,
								intent.getStringExtra("message"),
								Toast.LENGTH_SHORT).show();
						sharedPref.edit().putBoolean("my_first_time", false)
								.commit();
						first_login = false;
						startSetichat();
					} else {
						Toast.makeText(
								MainActivitySeTIChat.this,
								"Claves publicas regeneradas y subidas al servidor",
								Toast.LENGTH_LONG).show();
					}
					break;
				}
			}
		};
		IntentFilter createAccountMessageFilter = new IntentFilter();
		createAccountMessageFilter.addAction("es.uc3m.SeTIChat.CREATE_ACCOUNT");
		registerReceiver(createAccountReceiver, createAccountMessageFilter);

	}

	// CreateAccount nos devuelve nick y phone para realizar el registro del
	// nuevo usuario
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ACTION_SIGN_UP) {
			if (resultCode == RESULT_OK) {
				usuario = new User();
				usuario.nick = data.getStringExtra("nick");
				usuario.tlf = data.getStringExtra("tlf");
				MyCipher cifrador = new MyCipher();
				byte[][] keys = cifrador.generateRSAKeys();
				usuario.privateKey = Base64.encodeToString(keys[0], false);
				usuario.publicKey = Base64.encodeToString(keys[1], false);

				// Guardamos el usuario en la base de datos para que el
				// servicio pueda acceder a el
				try {
					dbhelper.UserSet.save(usuario);
				} catch (AdaFrameworkException e1) {
					Log.e("DB",
							"Ha habido un problema guardando el usuario en la base de datos.");
					e1.printStackTrace();
				}
				// Iniciamos el servicio
				try {
					startService(new Intent(MainActivitySeTIChat.this,
							SeTIChatService.class));
					bindService(new Intent(MainActivitySeTIChat.this,
							SeTIChatService.class), mConnection,
							Context.BIND_AUTO_CREATE);

				} catch (Exception e) {

					Log.d("MainActivity", "Unknown Error", e);

					stopService(new Intent(MainActivitySeTIChat.this,
							SeTIChatService.class));
				}

			}
		}
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		// Restore the previously serialized current tab position.
		if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
			getActionBar().setSelectedNavigationItem(
					savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// Serialize the current tab position.
		outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar()
				.getSelectedNavigationIndex());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.regenerar_claves) {
			MyCipher cifrador = new MyCipher();
			byte[][] keys = cifrador.generateRSAKeys();
			usuario.privateKey = Base64.encodeToString(keys[0], false);
			usuario.publicKey = Base64.encodeToString(keys[1], false);
			usuario.setStatus(Entity.STATUS_UPDATED);
			try {
				dbhelper.UserSet.save(usuario);
			} catch (AdaFrameworkException e) {
				e.printStackTrace();
			}
			// send our keys to server
			model.entities.Header header = new model.entities.Header(
					usuario.token, "setichat.appspot.com", UUID.randomUUID()
							.toString(), 9, false, false);
			model.entities.Upload uploadmessage = new model.entities.Upload(
					header, usuario.publicKey, "public");
			mService.sendMessage(parser.generateMessage(header, uploadmessage));
		} else {
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, show the tab contents in the
		// container view.
		if (tab.getPosition() == 0 && first_tab == true) {
			ContactsFragment fragment = new ContactsFragment();
			getFragmentManager().beginTransaction()
					.replace(R.id.container, fragment).commit();
			first_tab = false;
		}

	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	public void showException(Throwable t) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle("Exception!").setMessage(t.toString())
				.setPositiveButton("OK", null).show();
	}

	/** Defines callbacks for service binding, passed to bindService() */
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			// We've bound to LocalService, cast the IBinder and get
			// LocalService instance
			Log.i("Service Connection", "Estamos en onServiceConnected");
			SeTIChatServiceBinder binder = (SeTIChatServiceBinder) service;
			mService = binder.getService();

		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {

		}
	};

	public SeTIChatService getService() {
		return mService;
	}

	// SeTIChatServiceDelegate Methods

	public void showNotification(String message) {
		Context context = getApplicationContext();
		CharSequence text = message;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}

	private class SendMessageTask extends AsyncTask<String, Integer, Long> {
		ProgressDialog progress;

		@Override
		protected void onPreExecute() {
			progress = ProgressDialog.show(MainActivitySeTIChat.this,
					"Contactos",
					"Actualizando contactos del servidor, espere por favor...",
					false);
		}

		protected Long doInBackground(String... params) {
			ContactsHelper cHelper = new ContactsHelper(
					MainActivitySeTIChat.this);
			leerContactoDatabase();
			ArrayList<String> contactos = cHelper.getAllContacts();
			model.entities.Header header = new model.entities.Header(
					usuario.token, "setichat.appspot.com", UUID.randomUUID()
							.toString(), 2, false, false);
			model.entities.ContactRequest crContent = new model.entities.ContactRequest(
					header, contactos);
			mService.sendMessage(parser.generateMessage(header, crContent));
			return 0L;
		}

		protected void onPostExecute(Long result) {
			progress.dismiss();
		}
	}

}
