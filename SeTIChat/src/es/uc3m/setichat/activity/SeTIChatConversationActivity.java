package es.uc3m.setichat.activity;

import java.sql.Time;
import java.util.UUID;

import model.entities.ChatMessage;
import model.entities.Contact;
import model.entities.User;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fima.cardsui.views.CardUI;
import com.mobandme.ada.exceptions.AdaFrameworkException;

import es.uc3m.setichat.R;
import es.uc3m.setichat.helpers.DatabaseHelper;
import es.uc3m.setichat.helpers.MyCard;
import es.uc3m.setichat.helpers.MyCipher;
import es.uc3m.setichat.helpers.MySigner;
import es.uc3m.setichat.service.SeTIChatService;
import es.uc3m.setichat.service.SeTIChatServiceBinder;
import es.uc3m.setichat.utils.XMLParser;

/**
 * This activity will show the conversation with a given contact. It will allow
 * also to send him new messages and, of course, will refresh when a new message
 * arrives.
 * 
 * If the user is viewing a different conversation when a message arrive from a
 * third party contact, then a notification should be shown.
 * 
 * @author Guillermo Suarez de Tangil <guillermo.suarez.tangil@uc3m.es>
 * @author Jorge Blasco Alis <jbalis@inf.uc3m.es>
 */

public class SeTIChatConversationActivity extends Activity {

	private DatabaseHelper dbhelper;
	private Contact contacto;
	private User usuario;
	private ChatMessage cm;

	private Button send;
	private EditText edittext_mensaje;
	private CardUI mcardview;

	private boolean DEBUG = false;
	private boolean ENCRYPTED_SIGNED = false;

	private SeTIChatService mService;
	private BroadcastReceiver messageSendReceiver;
	private BroadcastReceiver messageRecvReceiver;

	private ServiceConnection mConnection = new ServiceConnection() {

		public void onServiceConnected(ComponentName className, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the service object we can use to
			// interact with the service. Because we have bound to a explicit
			// service that we know is running in our own process, we can
			// cast its IBinder to a concrete class and directly access it.
			mService = SeTIChatServiceBinder.getService();

			DEBUG = true;

			render();
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			// Because it is running in our same process, we should never
			// see this happen.

			if (DEBUG)
				Log.d("SeTIChatConversationActivity",
						"onServiceDisconnected: un-bounding the service");

			mService = null;
			Toast.makeText(SeTIChatConversationActivity.this, "Disconnected", // R.string.local_service_disconnected,
					Toast.LENGTH_SHORT).show();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e("Conversation", "oncreate");
		if (mService == null) {
			// Binding the activity to the service to get shared objects
			if (DEBUG)
				Log.d("SeTIChatConversationActivity", "Binding activity");
			bindService(new Intent(SeTIChatConversationActivity.this,
					SeTIChatService.class), mConnection,
					Context.BIND_AUTO_CREATE);
		} else {
			render();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.e("Conversation", "onresume");
		try {
			registerAllReceivers();
			mcardview.scrollToCard(mcardview.getLastCardStackPosition());

		} catch (Exception e) {
		}
	}

	private void unregisterAllReceivers() {
		try {
			unregisterReceiver(messageRecvReceiver);
			unregisterReceiver(messageSendReceiver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void registerAllReceivers() {
		// MENSAJE ENVIADO
		messageSendReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Toast.makeText(SeTIChatConversationActivity.this,
						intent.getStringExtra("message"), Toast.LENGTH_SHORT)
						.show();
				switch (intent.getIntExtra("code", -1)) {
				case 200:
					contacto.messages.add(cm);
					cm.setStatus(com.mobandme.ada.Entity.STATUS_UPDATED);
					contacto.setStatus(com.mobandme.ada.Entity.STATUS_UPDATED);
					try {
						dbhelper.contactSet.save();
					} catch (AdaFrameworkException e) {
						e.printStackTrace();
					}
					mcardview.addCard(new MyCard(usuario.nick, cm.message,
							"#f2a400", "#9d36d0", true, false));
					mcardview.refresh();
					mcardview
							.scrollToCard(mcardview.getLastCardStackPosition());
					break;
				default:
					break;
				}
			}
		};
		IntentFilter messageSentFilter = new IntentFilter();
		messageSentFilter.addAction("es.uc3m.SeTIChat.MESSAGE_SENT");
		registerReceiver(messageSendReceiver, messageSentFilter);

		// MENSAJE RECIBIDO
		messageRecvReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				int controlFirmado = intent.getIntExtra("code", 0);
				switch (controlFirmado) {
				case SeTIChatService.NO_FIRMADO:
					anadirCard(intent);
					Toast.makeText(SeTIChatConversationActivity.this,
							"Mensaje no cifrado no firmado", Toast.LENGTH_SHORT)
							.show();
					break;
				case SeTIChatService.FIRMADO_CORRECTO:
					anadirCard(intent);
					Toast.makeText(SeTIChatConversationActivity.this,
							"Mensaje cifrado firmado correctamente",
							Toast.LENGTH_SHORT).show();
					break;
				case SeTIChatService.FIRMADO_INCORRECTO:
					Toast.makeText(SeTIChatConversationActivity.this,
							"�Mensaje cifrado, firmado no verificado!",
							Toast.LENGTH_SHORT).show();
					break;
				default:
					Vibrator v = (Vibrator) SeTIChatConversationActivity.this
							.getSystemService(Context.VIBRATOR_SERVICE);
					v.vibrate(500);
					break;
				}

			}
		};
		IntentFilter messageRecvFilter = new IntentFilter();
		messageRecvFilter.addAction("es.uc3m.SeTIChat.MESSAGE_RECEIVED");
		registerReceiver(messageRecvReceiver, messageRecvFilter);
	}

	private void render() {
		setContentView(R.layout.activity_conversation);
		send = (Button) findViewById(R.id.button_send);
		// chat_list = (ListView) findViewById(R.id.listView_chat1);
		edittext_mensaje = (EditText) findViewById(R.id.editText_message);
		mcardview = (CardUI) findViewById(R.id.cardsview);
		mcardview.setSwipeable(true);

		try {
			dbhelper = new DatabaseHelper(SeTIChatConversationActivity.this);
			contacto = dbhelper.getContactbySourceId(getIntent()
					.getStringExtra("sourceID"));
			ENCRYPTED_SIGNED = getIntent().getBooleanExtra("encrypted", false);
			dbhelper.UserSet.fill();
			usuario = dbhelper.UserSet.get(0);
		} catch (AdaFrameworkException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (ChatMessage m : contacto.messages) {
			String origen = "";
			if (m.origin.equalsIgnoreCase(usuario.tlf)) {
				origen = usuario.nick;
				mcardview.addCard(new MyCard(origen, m.message, "#f2a400",
						"#9d36d0", true, false));
			} else {
				origen = contacto.nick;
				mcardview.addCard(new MyCard(origen, m.message, "#f22400",
						"#93f6d0", true, false));
			}

		}
		mcardview.refresh();
		mcardview.scrollToCard(mcardview.getLastCardStackPosition());

		send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				MyCipher cifrador = new MyCipher();
				MySigner firmador = new MySigner();
				Time time = new Time(System.currentTimeMillis());
				String message = edittext_mensaje.getText().toString() + " at "
						+ time;
				String ciphered_message = "";
				model.entities.ChatMessage chatmessage;

				// cabecera
				model.entities.Header header = new model.entities.Header(
						usuario.token, contacto.tlf, UUID.randomUUID()
								.toString(), 4, ENCRYPTED_SIGNED,
						ENCRYPTED_SIGNED);
				String signature = "";
				// firmado y cifrado, content y signature
				if (ENCRYPTED_SIGNED) {
					try {
						ciphered_message = cifrador.encrypt_message_AES(
								edittext_mensaje.getText().toString() + " at "
										+ time, contacto.publicKey);
						signature = firmador.sign_message(
								header.getIdDestination(),
								header.getIdMessage(), message,
								usuario.privateKey);
					} catch (Exception e) {
						e.printStackTrace();
					}
					chatmessage = new ChatMessage(header, (ciphered_message),
							signature);
				} else {
					chatmessage = new ChatMessage(header, message);
				}
				XMLParser parser = new XMLParser();
				mService.sendMessage(parser
						.generateMessage(header, chatmessage)); // Refresh
				cm = chatmessage;
				cm.origin = usuario.tlf;
				// restauramos el mensaje sin cifrar para ser mostrado cuando el
				// servidor notifique de su llegada.
				cm.message = message;

				edittext_mensaje.setText("");

			}
		});

	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.e("Conversation", "onpause");
		unregisterAllReceivers();
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.e("Conversation", "onstop");
		if (DEBUG)
			Log.d("SeTIChatConversationActivity", "Unbinding activity");

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.e("Conversation", "ondestroy");
		try {
			unbindService(mConnection);
		} catch (Exception e) {
			Log.e("NULLPOINTER", "UNBIND SIN SERVICIO REGISTRADO");
		}
	}

	private void anadirCard(Intent intent) {
		if (contacto.tlf.equalsIgnoreCase(intent.getStringExtra("contact"))) {
			mcardview.addCard(new MyCard(contacto.nick, intent
					.getStringExtra("message"), "#f22400", "#93f6d0", true,
					false));
			mcardview.refresh();
		}
		mcardview.scrollToCard(mcardview.getLastCardStackPosition());
	}

}
